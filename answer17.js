function division(integer1, integer2) {
    // format params to int
    let numberFormat1 = integer1.toFixed();
    let numberFormat2 = integer2.toFixed();

    return (numberFormat1/numberFormat2);
}

console.log(division(5, 2));
