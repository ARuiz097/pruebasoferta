// convert number base 10 to base k
function numberToBase(number, baseFormat) {
    // format params to int
    let numberFormat = number.toFixed();
    let newBaseFormat = baseFormat.toFixed();

    return Number(numberFormat).toString(newBaseFormat);    
}

console.log(numberToBase(287, 5));
